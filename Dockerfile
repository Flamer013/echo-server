FROM golang:latest

ENV GOBIN /go/bin
ENV PATH $PATH:$GOPATH/bin:$GOROOT/bin

RUN git clone https://Flamer013@bitbucket.org/Flamer013/echo-server.git /opt/echo-server/
RUN cd /opt/echo-server/ && git pull && go test && go build

EXPOSE 8800

CMD ["/opt/echo-server/echo-server"]